import sys

FutureCall = "TEST"
# sys.modules['__builtin__'].FutureCall = FutureCall

if ('__builtin__' not in sys.modules):
    sys.modules['__builtin__'] = {}
sys.modules['__builtin__']['FutureCall'] = FutureCall
print(sys.modules['__builtin__']['FutureCall'])
