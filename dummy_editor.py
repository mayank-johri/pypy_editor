#!/usr/bin/env python3
# -*- coding: utf-8 -*-
"""
Created on Tue May  1 07:38:34 2018

@author: mayank
"""

import wx   # requires wxPython
import wx.adv
import sys  # required to redirect the output


class EditorSashWindow(wx.Panel):
    def __init__(self, parent):
        wx.Panel.__init__(self, parent, -1)
        winids = []

        # Left window has fixed size and contains control buttons
        self.controls = wx.adv.SashLayoutWindow(self, -1)
        winids.append(self.controls.GetId())

        self.controls.SetDefaultSize((80, 600))
        self.controls.SetOrientation(wx.adv.LAYOUT_VERTICAL)
        self.controls.SetAlignment(wx.adv.LAYOUT_LEFT)

        b = wx.Button(self.controls, -1, "Open", (3, 20))
        self.Bind(wx.EVT_BUTTON, self.openFile, b)
        b.SetDefault()
        b2 = wx.Button(self.controls, -1, "Save", (3, 60))
        self.Bind(wx.EVT_BUTTON, self.saveFile, b2)
        b2.SetDefault()
        b3 = wx.Button(self.controls, -1, "Run", (3, 100))
        self.Bind(wx.EVT_BUTTON, self.run, b3)
        b3.SetDefault()
        b4 = wx.Button(self.controls, -1, "Clear", (3, 140))
        self.Bind(wx.EVT_BUTTON, self.clear, b4)
        b4.SetDefault()

        # This will occupy the space not used by the Layout Algorithm
        self.remainingSpace = wx.adv.SashLayoutWindow(self,
                                                      -1,
                                                      style=wx.NO_BORDER | wx.adv.SW_3D)

        self.python_editor = wx.TextCtrl(self.remainingSpace,
                                         -1, "", wx.DefaultPosition, wx.DefaultSize,
                                         wx.TE_MULTILINE | wx.SUNKEN_BORDER
                                         )
        self.python_editor.SetValue("#Editor window")

        # The output window is at the extreme right
        win = wx.adv.SashLayoutWindow(self,
                                      -1,
                                      wx.DefaultPosition, (200, 30),
                                      wx.NO_BORDER | wx.adv.SW_3D
                                      )
        winids.append(win.GetId())
        win.SetDefaultSize((300, 600))
        win.SetOrientation(wx.adv.LAYOUT_VERTICAL)
        win.SetAlignment(wx.adv.LAYOUT_RIGHT)
        win.SetSashVisible(wx.adv.SASH_LEFT, True)
        win.SetExtraBorderSize(10)
        self.rightWindow = win
        self.output_window = wx.TextCtrl(win, -1, "", wx.DefaultPosition,
                                         wx.DefaultSize, wx.TE_MULTILINE | wx.SUNKEN_BORDER)
        self.output_window.SetValue("Output Window\n")
        # redirecting output
        sys.stdout = self.output_window
        sys.stderr = self.output_window

        self.Bind(wx.EVT_SASH_DRAGGED_RANGE, self.OnSashDrag, id=min(winids),
                  id2=max(winids))
        self.Bind(wx.EVT_SIZE, self.OnSize)

    def OnSashDrag(self, event):
        eobj = event.GetEventObject()
        if eobj is self.rightWindow:
            self.rightWindow.SetDefaultSize((event.GetDragRect().width, 600))
        wx.LayoutAlgorithm().LayoutWindow(self, self.remainingSpace)
        self.remainingSpace.Refresh()

    def OnSize(self, event):
        wx.LayoutAlgorithm().LayoutWindow(self, self.remainingSpace)

    def saveFile(self, event):
        self.output_window.SetValue("Save File not implemented")

    def openFile(self, event):
        self.output_window.SetValue("Open File not implemented")

    def run(self, event):
        '''Runs the user code; input() and raw_input() are implemented
           with dialogs.'''
        user_code = self.python_editor.GetValue()
        myGlobals = globals()
        myGlobals['raw_input'] = self.myRawInput
        myGlobals['input'] = self.myInput
        exec(user_code in myGlobals)

    def clear(self, event):
        '''Clears the output window'''
        self.output_window.SetValue("")

    def myRawInput(self, text):
        dlg = wx.TextEntryDialog(self, text, 'raw_input() request', '')
        if dlg.ShowModal() == wx.ID_OK:
            user_response = dlg.GetValue()
        dlg.Destroy()
        return user_response

    def myInput(self, text):
        dlg = wx.TextEntryDialog(self, text, 'input() request', '')
        if dlg.ShowModal() == wx.ID_OK:
            user_response = dlg.GetValue()
        dlg.Destroy()
        return eval(user_response)

# class MainWindow(wx.Frame):
#    def __init__(self, parent, title):
##        super(MainWindow, self).__init__(parent, title=title)
# wx.Frame.__init__(self,parent, -1, title, size=(800, 600),
# style=wx.DEFAULT_FRAME_STYLE|wx.NO_FULL_REPAINT_ON_RESIZE)
#                # Here we create a panel and a notebook on the panel
#        wx.Frame.__init__(self, parent, title=title)
#        p = wx.Panel(self)
#        nb = wx.Notebook(p)
#
#        page1 = EditorSashWindow(nb)
#        nb.AddPage(page1)
#        sizer = wx.BoxSizer()
#        sizer.Add(nb, 1, wx.EXPAND)
#        p.SetSizer(sizer)


# def main():
#
#    app = wx.App()
#    ex = MainWindow(None, title='Go To Class')
#    ex.Show()
#    app.MainLoop()
#
#
# if __name__ == '__main__':
#    main()

#
# if __name__ == "__main__":
#    app = wx.App()
#    MainWindow( None, title="Simple Notebook Example").Show()
#    app.MainLoop()


class PageOne(wx.Panel):
    def __init__(self, parent):
        wx.Panel.__init__(self, parent)
        wx.StaticText(self, -1, "This is a PageOne object", (20, 20))


class MainFrame(wx.Frame):
    def __init__(self):
        wx.Frame.__init__(self, None, title="Simple Notebook Example")

        # Here we create a panel and a notebook on the panel
        p = wx.Panel(self)
        nb = wx.Notebook(p)

        # create the page windows as children of the notebook
        page1 = EditorSashWindow(nb)

        # add the pages to the notebook with the label to show on the tab
        nb.AddPage(page1, "Page 1")

        # finally, put the notebook in a sizer for the panel to manage
        # the layout
        sizer = wx.BoxSizer()
        sizer.Add(nb, 1, wx.EXPAND)
        p.SetSizer(sizer)


if __name__ == "__main__":
    app = wx.App()
    MainFrame().Show()
    app.MainLoop()
