find . -name '*.py'|grep -v migrations|xargs autoflake --in-place --remove-all-unused-imports --remove-unused-variables
autopep8 --in-place --recursive --max-line-length=100  --exclude="*/migrations/*" .


find . -name '*.py'|grep -v migrations|xargs autoflake --in-place --remove-all-unused-imports
flake8 --statistics --count -qq
autopep8 --in-place --recursive --max-line-length=100 --exclude="*/migrations/*" --select="W291,W293" .
